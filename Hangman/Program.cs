﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    internal class Program
    {
        static void Main(string[] args)
        {
            HangmanGame game = new HangmanGame();

            bool playAgain = true;
            while (playAgain)
            {
                game.Play();

                playAgain = AskPlayAgain();
            }
        }

        static bool AskPlayAgain()
        {
            while (true)
            {
                Console.WriteLine("Would you like to play again? Enter y for yes or n for no.");
                string answer = Console.ReadLine();
                if (answer.ToLower() == "y")
                {
                    return true;
                }
                else if (answer.ToLower() == "n")
                {
                    return false;
                }
                Console.WriteLine("Invalid input: Try again.");
            }
        }
    }
}
