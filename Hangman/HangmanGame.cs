﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    internal class HangmanGame
    {
        static readonly string[] _hangmanPics = {
            "  + ---+\n  |   |\n      |\n      |\n      |\n      |\n=========",
            "  +---+\n  |   |\n  O   |\n      |\n      |\n      |\n=========",
            "  +---+\n  |   |\n  O   |\n  |   |\n      |\n      |\n=========",
            "  +---+\n  |   |\n  O   |\n /|   |\n      |\n      |\n=========",
            "  +---+\n  |   |\n  O   |\n /|\\  |\n      |\n      |\n=========",
            "  +---+\n  |   |\n  O   |\n /|\\  |\n /    |\n      |\n=========",
            "  +---+\n  |   |\n  O   |\n /|\\  |\n / \\  |\n      |\n========="};

        int _totalChances => _hangmanPics.Length - 1;

        List<string> _words = new List<string>();
        Random _rnd = new Random();

        public HangmanGame()
        {
            string wordList = Properties.Resources.wordlist;

            using (System.IO.StringReader reader = new System.IO.StringReader(wordList))
            {
                string word;
                while ((word = reader.ReadLine()) != null)
                {
                    _words.Add(word.ToUpper());
                }
            }
        }

        public void Play()
        {
            string solution = _words[_rnd.Next(_words.Count)];
            List<char> lettersLeft = GetUniqueLetters(solution);

            int failCount = 0;
            List<char> guessedChars = new List<char>();

            while (failCount < _totalChances)
            {
                Console.Clear();
                PrintHangmanPic(failCount);
                PrintWord(solution, guessedChars);
                PrintGuessedCharacters(guessedChars);

                if (lettersLeft.Count == 0)
                {
                    Console.WriteLine($"\nCongratulations! You won Hangman by correctly guessing the word {solution}");
                    return;
                }

                bool guessValid = false;

                while (!guessValid)
                {
                    Console.Write("Please pick a letter: ");
                    string guess = Console.ReadLine();

                    if (!IsAlpha(guess))
                    {
                        Console.WriteLine("Invalid guess: Your guess is not a letter.");
                        continue;
                    }

                    char guessChar = guess.ToUpper()[0];

                    if (guessedChars.Contains(guessChar))
                    {
                        Console.WriteLine("Invalid guess: You have already guessed that.");
                        continue;
                    }

                    if (solution.Contains(guessChar))
                    {
                        lettersLeft.Remove(guessChar);
                    }
                    else
                    {
                        failCount++;
                    }

                    guessedChars.Add(guessChar);
                    guessedChars.Sort();
                    guessValid = true;
                }
            }

            Console.Clear();
            PrintHangmanPic(failCount);
            Console.WriteLine($"\nSorry, you lost Hangman! The word was {solution}.");
        }

        private void PrintHangmanPic(int guessCount)
        {
            Console.WriteLine("\n======================\n");
            Console.WriteLine(_hangmanPics[guessCount]);
        }

        private void PrintWord(string word, List<char> guessedChars)
        {
            for (int i = 0; i < word.Length; i++)
            {
                char letter = word[i];
                if (guessedChars.Contains(letter))
                {
                    Console.Write(("" + letter).ToUpper());
                }
                else
                {
                    Console.Write("_");
                }
                if (i < word.Length - 1)
                {
                    Console.Write(" ");
                }
            }

            Console.WriteLine();
        }

        private void PrintGuessedCharacters(List<char> guessedChars)
        {
            Console.Write("Guessed letters: ");

            for (int i = 0; i < guessedChars.Count; i++)
            {
                char letter = guessedChars[i];

                Console.Write(("" + letter).ToUpper());

                if (i < guessedChars.Count - 1)
                {
                    Console.Write(" ");
                }
            }

            Console.WriteLine();
        }

        private bool IsAlpha(string s)
        {
            if (s.Length != 1) return false;
            char c = s.ToUpper()[0];
            return c >= 'A' && c <= 'Z';
        }

        private List<char> GetUniqueLetters(string word)
        {
            List<char> letters = new List<char>();
            foreach (char aCharacter in word)
            {
                if (letters.Contains(aCharacter)) continue;
                letters.Add(aCharacter);
            }

            return letters;
        }
    }
}
